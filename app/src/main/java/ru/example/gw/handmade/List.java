package ru.example.gw.handmade;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class List extends ActionBarActivity {
    public DataBaseHelper mDatabaseHelper;
    public SQLiteDatabase mDataBase;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        mDatabaseHelper = new DataBaseHelper(this);
        mDataBase = mDatabaseHelper.getWritableDatabase();

        cursor = mDataBase.query(mDatabaseHelper.TABLE_NAME, new String[]{
                        mDatabaseHelper.UID, mDatabaseHelper.NAME}, null,
                null,
                null,
                null,
                null
        );

        java.util.List<String> list = new ArrayList<>();
        final ListView listView = (ListView) findViewById(R.id.list);

        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(mDatabaseHelper.UID));
            String name = cursor.getString(cursor.getColumnIndex(mDatabaseHelper.NAME));

            list.add(name);

            // Создаем адаптер
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, list);

            dataAdapter
                    .setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            dataAdapter.notifyDataSetChanged();
            listView.setAdapter(dataAdapter);
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {
                TextView textView = (TextView) itemClicked;
                //String strText = textView.getText().toString(); // получаем текст нажатого элемента

                //if(strText.equalsIgnoreCase(getResources().getString(R.string.name))) {
                    // Запускаем активность, связанную с определенным именем кота
                    Intent intent = new Intent(List.this, info.class);
                    intent.putExtra("name", textView.getText().toString());
                    startActivity(intent);
                    //startActivity(new Intent(this, info.class));
                }
            //}
        });
        cursor.close();
    }
    public void Back_Main(View view) {
        Intent intent = new Intent(List.this, MainActivity.class);
        startActivity(intent);
    }


}

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
*/