package ru.example.gw.handmade;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class info extends ActionBarActivity {

    public DataBaseHelper mDatabaseHelper;
    public SQLiteDatabase mDataBase;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        mDatabaseHelper = new DataBaseHelper(this);
        mDataBase = mDatabaseHelper.getWritableDatabase();
        String name_out;
        name_out = getIntent().getExtras().getString("name");



        cursor = mDataBase.query(mDatabaseHelper.TABLE_NAME, new String[]{
                        mDatabaseHelper.UID, mDatabaseHelper.NAME, mDatabaseHelper.PRICE,
                        mDatabaseHelper.MATERIAL, mDatabaseHelper.DESCRIPTION, mDatabaseHelper.FILE}, null,
                null,
                null,
                null,
                null
        );

        //java.util.List<String> list = new ArrayList<>();
        //Spinner spinner = (Spinner) findViewById(R.id.spinner);
        //final TextView listView = (TextView) findViewById(R.id.list);
        java.util.List<String> list = new ArrayList<>();
        TextView Name_i = (TextView) findViewById(R.id.name_i);
        TextView Price_i = (TextView) findViewById(R.id.price_i);
        TextView Material_i = (TextView) findViewById(R.id.material_i);
        TextView Desc_i = (TextView) findViewById(R.id.description_i);
        ImageView Photo = (ImageView) findViewById(R.id.Photo);

        String name="";
        String price="";
        String material="";
        String discription="";
        String file="";


        while (cursor.moveToNext()) {

            name = cursor.getString(cursor.getColumnIndex(mDatabaseHelper.NAME));
            price = cursor.getString(cursor.getColumnIndex(mDatabaseHelper.PRICE));
            material = cursor.getString(cursor.getColumnIndex(mDatabaseHelper.MATERIAL));
            discription = cursor.getString(cursor.getColumnIndex(mDatabaseHelper.DESCRIPTION));
            Log.d("F","FILE--"+file);
            file = cursor.getString(cursor.getColumnIndex(mDatabaseHelper.FILE));

            if (name.equals(name_out))
                break;

        }

        //Uri outf= Uri.parse(file);
        Name_i.setText(name);
        Price_i.setText(price);
        Material_i.setText(material);
        Desc_i.setText(discription);
        Log.d("F","FILE--"+file);
        Photo.setImageURI(Uri.parse(file));


        cursor.close();
    }
    public void Info_back(View view) {
        Intent intent = new Intent(info.this, List.class);
        startActivity(intent);}


}

        //TextView infoTextView = (TextView)findViewById(R.id.names);
        //infoTextView.setText(user);



  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
*/