package ru.example.gw.handmade;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Grey_Wolf on 02.06.2015.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    // константы для конструктора
    public static final String DATABASE_NAME = "database.db";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "Handmade_table";
    public static final String UID = "_id";
    public static final String NAME = "Name";
    public static final String PRICE = "Prices";
    public static final String MATERIAL = "Material";
    public static final String DESCRIPTION ="description";
    public static final String FILE ="file";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_ENTRIES = "CREATE TABLE "
                + TABLE_NAME + "(" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + NAME + " TEXT," +  PRICE + " TEXT," + MATERIAL + " TEXT," +  DESCRIPTION + " TEXT," + FILE + " TEXT " + ")";
        db.execSQL(SQL_CREATE_ENTRIES);
    }


  //  private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
    //        + TABLE_NAME;

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);

    }
}