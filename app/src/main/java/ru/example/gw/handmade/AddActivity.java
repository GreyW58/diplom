package ru.example.gw.handmade;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;


public class AddActivity extends ActionBarActivity {

    File directory;
    final int TYPE_PHOTO = 1;

    final int REQUEST_CODE_PHOTO = 1;

    final String TAG = "myLogs";
    File file;

    ImageView ivPhoto;
    ContentValues cv = new ContentValues();


    public EditText Name;
    public EditText Price;
    public EditText Material;
    public EditText Description;
    public DataBaseHelper mDatabaseHelper;
    public SQLiteDatabase mDataBase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        createDirectory();
        //ivPhoto = (ImageView) findViewById(R.id.pre_fot);


        Name = (EditText) findViewById(R.id.name);
        Price = (EditText) findViewById(R.id.price);
        Material = (EditText) findViewById(R.id.material);
        Description = (EditText) findViewById(R.id.description);
        // Инициализируем класс-обёртку
        mDatabaseHelper = new DataBaseHelper(this);

        // База для записи и чтения
        mDataBase = mDatabaseHelper.getWritableDatabase();

    }
    public void onClickPhoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, generateFileUri(TYPE_PHOTO));
        startActivityForResult(intent, REQUEST_CODE_PHOTO);

        Log.d(TAG, "DFGHJKL="+file);
        String pput=file.toString();
        Log.d(TAG, "DF111="+pput);
        File qqw=file;
        //String qqwe=qqw.toString();

        Uri photodir1 = Uri.fromFile(file);
        String photodir = photodir1.toString();

        cv.put(mDatabaseHelper.FILE, photodir);
        Log.d(TAG,"RDFCHBJKNHHHHHHH==="+photodir);
        TextView Pu = (TextView) findViewById(R.id.put);
        Pu.setText(photodir);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == REQUEST_CODE_PHOTO) {
            if (resultCode == RESULT_OK) {
                if (intent == null) {
                    Log.d(TAG, "Intent is null");

                } else {
                    Log.d(TAG, "Photo uri: " + intent.getData());
                    Bundle bndl = intent.getExtras();
                    if (bndl != null) {
                        Object obj = intent.getExtras().get("data");
                        if (obj instanceof Bitmap) {
                            Bitmap bitmap = (Bitmap) obj;
                            Log.d(TAG, "bitmap " + bitmap.getWidth() + " x "
                                    + bitmap.getHeight());
                            ivPhoto.setImageBitmap(bitmap);
                        }
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "Canceled");
            }
        }
    }

    public Uri generateFileUri(int type) {
        file = null;

                file = new File(directory.getPath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");

        Log.d(TAG, "fileName = " + file);
        return Uri.fromFile(file);
    }


    public void createDirectory() {
        directory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyFolder");
        if (!directory.exists())
            directory.mkdirs();
    }

    //TextView Put = (TextView) findViewById(R.id.put);
    //Put.setText(photodir);




    public void Click_Save(View v) {



        cv.put(mDatabaseHelper.NAME, Name.getText().toString());
        cv.put(mDatabaseHelper.PRICE, Price.getText().toString());
        cv.put(mDatabaseHelper.MATERIAL, Material.getText().toString());
        cv.put(mDatabaseHelper.DESCRIPTION, Description.getText().toString());
        //cv.put(mDatabaseHelper.FILE, file.getText().toString());
        // вызываем метод вставки
        mDataBase.insert(mDatabaseHelper.TABLE_NAME, null, cv);
        //mDataBase.insert(DataBaseHelper.TABLE_NAME, DataBaseHelper.PRICE, cv);
        Name.setText("");
        Price.setText("");
        Material.setText("");
        Description.setText("");

        // закрываем соединения с базой данных
        //db.close();
        //sqlHelper.close();

    }
    public void  Click_Read(View v){

        Cursor cursor = mDataBase.query(mDatabaseHelper.TABLE_NAME, new String[]{
                        mDatabaseHelper.UID, mDatabaseHelper.FILE}, null,
                null,
                null,
                null,
                null
        );
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(mDatabaseHelper.UID));
            String name = cursor.getString(cursor
                    .getColumnIndex(mDatabaseHelper.FILE));
            Log.i("LOG_TAG", "ROW " + id + " HAS NAME " + name);
        }
        cursor.close();
    }
    public void Back_main(View view) {
        Intent intent = new Intent(AddActivity.this, MainActivity.class);
        startActivity(intent);}
}
   /* @Override
    protected void onStop() {
        super.onStop();
        // закрываем соединения с базой данных
        mDataBase.close();
        mDatabaseHelper.close();
    }

   /* @Override
    protected void onStop() {
        super.onStop();
        // закрываем соединения с базой данных
        db.close();
        sqlHelper.close();
    }*/




    //@Override
  /*  public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}*/
