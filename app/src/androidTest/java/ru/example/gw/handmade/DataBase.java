package ru.example.gw.handmade;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Grey_Wolf on 02.06.2015.
 */
public interface DataBase {
    void onCreate(SQLiteDatabase db);

    void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);
}
